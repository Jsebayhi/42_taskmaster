package main

import (
	"errors"
	"log"
	"os"
	"strings"
	"sync"
)

const (
	dash string = " - "
)

var (
	ErrorBadSuffix = errors.New("Bad suffix.")
)

type TaskMaster struct {
	TasksProcesses map[string]map[int]*TaskProcess //map[taskName][processId]*TaskConfig
	TasksConfigs   map[string]*TaskConfig          // map[ConfigName]TaskConfig

	taskConfigFileName string
	LogFileName        string
	LogFile            *os.File
	launcherLock       sync.Mutex //Locker to avoid context conflict when starting multiple process.
}

//
// Taskmaster initialization and termination
//

// return:
// _______ true: an error occurred
// _______ false: no error occurred
func (taskMaster *TaskMaster) Init(taskConfFilePath string) bool { //TODO clarify
	var err error = nil

	// Log initialization
	taskMaster.LogFileName = logsFileDefault
	var LogFilePath = strings.Split(taskMaster.LogFileName, "/")
	if err = os.MkdirAll(strings.Join(LogFilePath[:len(LogFilePath)-1], "/"), os.ModePerm); err != nil {
		LogInitializingError("log system", err.Error())
	}
	if taskMaster.LogFile, err = os.OpenFile(taskMaster.LogFileName, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644); err != nil {
		LogInitializingError("log system", err.Error())
	}
	log.SetOutput(taskMaster.LogFile)
	log.SetFlags(log.Ldate | log.Lmicroseconds | log.LUTC)
	// TaskConfigs initialization
	if err = taskMaster.TaskConfigFileNameSet(taskConfFilePath); err != nil {
		LogInitializingError("Tasks config path:"+taskConfFilePath, err.Error())
	}
	if err = taskMaster.LoadTaskConfigs(); err != nil {
		LogInitializingError("Tasks config path:"+taskConfFilePath, err.Error())
	}
	// Process initialization
	taskMaster.TasksProcesses = map[string]map[int]*TaskProcess{}
	taskMaster.TasksConfigs, err = extractConf(g_taskMaster.TaskConfigFileName())
	if err != nil {
		log.Fatal(err)
	}
	actionTasksInit()
	return err != nil
}

func (taskMaster *TaskMaster) Terminate() error {
	taskMaster.LogFile.Close()
	return nil
}

//
// Methods
//

func (taskMaster *TaskMaster) LoadTaskConfigs() error {
	var err error
	if _, err := os.Stat(taskMaster.TaskConfigFileName()); os.IsNotExist(err) {
		return os.ErrNotExist
	}
	if taskMaster.TasksConfigs, err = extractConf(taskMaster.TaskConfigFileName()); err != nil {
		return err
	}
	return nil
}

func (taskMaster *TaskMaster) ProcessGetAll() (processes []*TaskProcess) { //Todo externalise
	for _, task := range g_taskMaster.TasksProcesses {
		for _, process := range task {
			processes = append(processes, process)
		}
	}
	return
}

func (taskMaster *TaskMaster) TasksProcessesGetAll(taskName string) (processes []*TaskProcess, err error) { //Todo externalise
	if task, err := taskMaster.FindTask(taskName); err != nil {
		return nil, err
	} else {
		for _, process := range task {
			processes = append(processes, process)
		}
	}
	return
}

func (taskMaster *TaskMaster) FindProcess(taskName string, processId int) (*TaskProcess, error) { //Todo externalise
	if process, DoesExist := taskMaster.TasksProcesses[taskName][processId]; DoesExist {
		return process, nil
	}
	return nil, ErrorNew_ErrorUnableToFindTaskProcess(taskName, processId)
}

func (taskMaster *TaskMaster) FindTask(taskName string) (map[int]*TaskProcess, error) { //Todo externalise
	if task, DoesExist := taskMaster.TasksProcesses[taskName]; DoesExist {
		return task, nil
	}
	return nil, ErrorNew_ErrorUnableToFindTask(taskName)
}

// Lock handling

func (taskMaster *TaskMaster) LockProcessLauncher() {
	taskMaster.launcherLock.Lock()
	LogProcessLauncher("Lock on")
}

func (taskMaster *TaskMaster) UnLockProcessLauncher() {
	taskMaster.launcherLock.Unlock()
	LogProcessLauncher("Lock off")
}

//
// GETTER && SETTER
//

// TaskConfigFileName
func (taskMaster *TaskMaster) TaskConfigFileName() string {
	return taskMaster.taskConfigFileName
}

func (taskMaster *TaskMaster) TaskConfigFileNameSet(newConfigPath string) error {
	if !strings.HasSuffix(newConfigPath, TasksConfSuffix) {
		return ErrorBadSuffix
	}
	if _, err := os.Stat(newConfigPath); os.IsNotExist(err) {
		return os.ErrNotExist
	} else {
		taskMaster.taskConfigFileName = newConfigPath
	}
	return nil
}
