package main

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

func extractConf(yamlFilePath string) (taskes map[string]*TaskConfig, err error) {
	taskes = map[string]*TaskConfig{}

	yamlFile, err := ioutil.ReadFile(yamlFilePath)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(yamlFile, &taskes)
	if err != nil {
		return nil, err
	}
	return taskes, nil
}
