package main

import (
	"strconv"

	"gopkg.in/abiosoft/ishell.v1"
	"text/tabwriter"
	"os"
	"fmt"
	"sort"
	"strings"
)

const (
	ShellCommand_status = "status"
	HandlerStatusSyntax string = string(ShellCommand_status)+" "+ArgProcessExtractorSyntax
	tab_separator string = "\t"
	tab_viewSeparator string = "| "
	tab_viewIntersect string = "+ "
	tab_newLine string = "\n"
)

func processFormatStatus(taskProcess *TaskProcess) (msg string) {
	HandlerSyntax[ShellCommand_status] = HandlerStatusSyntax
	actionCmdsProcessUpdateData(taskProcess)
	msg += tab_viewSeparator + taskProcess.taskConfig.Name
	msg += tab_separator + tab_viewSeparator + taskProcess.ProcessIdAsString()
	msg += tab_separator + tab_viewSeparator + string(taskProcess.status)
	if taskProcess.IsActive() {
		msg += tab_separator + tab_viewSeparator + strconv.Itoa(taskProcess.Process.Pid)
	} else {
		msg += tab_separator + tab_viewSeparator
	}
	msg += tab_separator + tab_viewSeparator + taskProcess.TimeToLaunch.String()
	msg += tab_separator + tab_viewSeparator + taskProcess.TimeAlive().String()
	msg += tab_separator + tab_viewSeparator + taskProcess.taskConfig.StdoutLogfile
	msg += tab_separator + tab_viewSeparator + taskProcess.taskConfig.StderrLogfile
	msg += tab_separator + tab_viewSeparator + tab_newLine
	return
}

func handlerStatus(shell *ishell.Shell) {

	shell_Register(shell, ShellCommand_status, func(args ...string) (string, error) {
		msg := tab_viewSeparator + "Name"
		msg += tab_separator + tab_viewSeparator + "Id"
		msg += tab_separator + tab_viewSeparator + "Status"
		msg += tab_separator + tab_viewSeparator + "Pid"
		msg += tab_separator + tab_viewSeparator + "Time To Launch"
		msg += tab_separator + tab_viewSeparator + "Time Alive"
		msg += tab_separator + tab_viewSeparator + "Stdout"
		msg += tab_separator + tab_viewSeparator + "Stderr"
		msg += tab_separator + tab_viewSeparator + tab_newLine
		// separating line
		msg += tab_viewIntersect
		msg += tab_separator + tab_viewIntersect
		msg += tab_separator + tab_viewIntersect
		msg += tab_separator + tab_viewIntersect
		msg += tab_separator + tab_viewIntersect
		msg += tab_separator + tab_viewIntersect
		msg += tab_separator + tab_viewIntersect
		msg += tab_separator + tab_viewIntersect
		msg += tab_separator + tab_viewIntersect + tab_newLine
		// setting default command
		if len(args) == 0 {
			args = append(args, ShellCommand_parameterAll)
		}
		//executing commands
		tasksProcesses, msgs := ArgExtractorProcess(args...)
		// sorting TaskProcess by name
		tasksProcessesMap := map[string]*TaskProcess{}
		for _, taskProcess := range tasksProcesses {
			tasksProcessesMap[taskProcess.FullName()] = taskProcess
		}
		var mapIndex = []string{}
		for key, _ := range tasksProcessesMap {
			mapIndex = append(mapIndex, key)
		}
		sort.Strings(mapIndex)
		for _, key := range mapIndex {
			msg += processFormatStatus(tasksProcessesMap[key])
		}
		// Displaying
		w := new(tabwriter.Writer)
		w.Init(os.Stdout, 0, 4, 1, ' ', 0)
		tab := strings.Split(msg, tab_newLine)
		for _, line := range tab {
			fmt.Fprintln(w, line)
		}
		w.Flush()
		return strings.Join(msgs, OutputEventSeparator), nil
	})
}
