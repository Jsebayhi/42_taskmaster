package main

import (
	"testing"
	"fmt"
)

func stringSliceComparison(a1, a2 []string) bool {
	if len(a1) != len(a2) {
		return false
	}
	hasBeenFound := false
	for _, v1 := range a1 {
		hasBeenFound = false
		for _, v2 := range a2 {
			if v1 == v2 {
				hasBeenFound = true
				break
			}
		}
		if hasBeenFound != true {
			return false
		}
	}
	return true
}

func TestProcessesToKillKeepRestart(t *testing.T) {
	{
		parameterOldMap := map[string]*TaskConfig{"task_1":{}}
		parameterNewMap := map[string]*TaskConfig{}
		expectedProcessesToKill := []string{"task_1"}
		expectedProcessesToKeep := []string{}
		expectedProcessesToStart := []string{}
		expectedProcessesToRestart := []string{}
		toKill, toKeep, toStart, toRestart := ProcessesToKillKeepRestart(parameterOldMap, parameterNewMap)
		logMsg := fmt.Sprintf("toKill: expected '%v', got '%v'. " +
			"toKeep: expected '%v', got '%v'. " +
			"toStart: expected '%v', got '%v'. " +
			"toRestart: expected '%v', got '%v'. ", expectedProcessesToKill, toKill, expectedProcessesToKeep, toKeep, expectedProcessesToStart, toStart, expectedProcessesToRestart, toRestart)
		if !stringSliceComparison(toKill, expectedProcessesToKill) ||
			!stringSliceComparison(toKeep, expectedProcessesToKeep) ||
			!stringSliceComparison(toStart, expectedProcessesToStart) ||
			!stringSliceComparison(toRestart, expectedProcessesToRestart) {
			t.Errorf(logMsg)
		} else {
			t.Log(logMsg) // Allow to know what is happening when using go test -v
		}
	}
	{
		parameterOldMap := map[string]*TaskConfig{"task_1":{}, "task_2":{}}
		parameterNewMap := map[string]*TaskConfig{"task_1":{}, "task_2":{}}
		expectedProcessesToKill := []string{}
		expectedProcessesToKeep := []string{"task_1", "task_2"}
		expectedProcessesToStart := []string{}
		expectedProcessesToRestart := []string{}
		toKill, toKeep, toStart, toRestart := ProcessesToKillKeepRestart(parameterOldMap, parameterNewMap)
		logMsg := fmt.Sprintf("toKill: expected '%v', got '%v'. " +
			"toKeep: expected '%v', got '%v'. " +
			"toStart: expected '%v', got '%v'. " +
			"toRestart: expected '%v', got '%v'. ", expectedProcessesToKill, toKill, expectedProcessesToKeep, toKeep, expectedProcessesToStart, toStart, expectedProcessesToRestart, toRestart)
		if !stringSliceComparison(toKill, expectedProcessesToKill) ||
			!stringSliceComparison(toKeep, expectedProcessesToKeep) ||
			!stringSliceComparison(toStart, expectedProcessesToStart) ||
			!stringSliceComparison(toRestart, expectedProcessesToRestart) {
			t.Errorf(logMsg)
		} else {
			t.Log(logMsg)
		}
	}
	{
		parameterOldMap := map[string]*TaskConfig{"task_1":{ProcessNb: 1}}
		parameterNewMap := map[string]*TaskConfig{"task_1":{ProcessNb: 0}}
		expectedProcessesToKill := []string{}
		expectedProcessesToKeep := []string{}
		expectedProcessesToStart := []string{}
		expectedProcessesToRestart := []string{"task_1"}
		toKill, toKeep, toStart, toRestart := ProcessesToKillKeepRestart(parameterOldMap, parameterNewMap)
		logMsg := fmt.Sprintf("toKill: expected '%v', got '%v'. " +
			"toKeep: expected '%v', got '%v'. " +
			"toStart: expected '%v', got '%v'. " +
			"toRestart: expected '%v', got '%v'. ", expectedProcessesToKill, toKill, expectedProcessesToKeep, toKeep, expectedProcessesToStart, toStart, expectedProcessesToRestart, toRestart)
		if !stringSliceComparison(toKill, expectedProcessesToKill) ||
			!stringSliceComparison(toKeep, expectedProcessesToKeep) ||
			!stringSliceComparison(toStart, expectedProcessesToStart) ||
			!stringSliceComparison(toRestart, expectedProcessesToRestart) {
			t.Errorf(logMsg)
		} else {
			t.Log(logMsg)
		}
	}
	{
		parameterOldMap := map[string]*TaskConfig{}
		parameterNewMap := map[string]*TaskConfig{"task_1":{}}
		expectedProcessesToKill := []string{}
		expectedProcessesToKeep := []string{}
		expectedProcessesToStart := []string{"task_1"}
		expectedProcessesToRestart := []string{}
		toKill, toKeep, toStart, toRestart := ProcessesToKillKeepRestart(parameterOldMap, parameterNewMap)
		logMsg := fmt.Sprintf("toKill: expected '%v', got '%v'. " +
			"toKeep: expected '%v', got '%v'. " +
			"toStart: expected '%v', got '%v'. " +
			"toRestart: expected '%v', got '%v'. ", expectedProcessesToKill, toKill, expectedProcessesToKeep, toKeep, expectedProcessesToStart, toStart, expectedProcessesToRestart, toRestart)
		if !stringSliceComparison(toKill, expectedProcessesToKill) ||
			!stringSliceComparison(toKeep, expectedProcessesToKeep) ||
			!stringSliceComparison(toStart, expectedProcessesToStart) ||
			!stringSliceComparison(toRestart, expectedProcessesToRestart) {
			t.Errorf(logMsg)
		} else {
			t.Log(logMsg)
		}
	}
	{
		parameterOldMap := map[string]*TaskConfig{"task_1":{}, "task_2":{ProcessNb: 1}, "task_3":{}}
		parameterNewMap := map[string]*TaskConfig{"task_1":{}, "task_2":{ProcessNb: 0}, "new_task_1":{}}
		expectedProcessesToKill := []string{"task_3"}
		expectedProcessesToKeep := []string{"task_1"}
		expectedProcessesToStart := []string{"new_task_1"}
		expectedProcessesToRestart := []string{"task_2"}
		toKill, toKeep, toStart, toRestart := ProcessesToKillKeepRestart(parameterOldMap, parameterNewMap)
		logMsg := fmt.Sprintf("toKill: expected '%v', got '%v'. " +
			"toKeep: expected '%v', got '%v'. " +
			"toStart: expected '%v', got '%v'. " +
			"toRestart: expected '%v', got '%v'. ", expectedProcessesToKill, toKill, expectedProcessesToKeep, toKeep, expectedProcessesToStart, toStart, expectedProcessesToRestart, toRestart)
		if !stringSliceComparison(toKill, expectedProcessesToKill) ||
			!stringSliceComparison(toKeep, expectedProcessesToKeep) ||
			!stringSliceComparison(toStart, expectedProcessesToStart) ||
			!stringSliceComparison(toRestart, expectedProcessesToRestart) {
			t.Errorf(logMsg)
		} else {
			t.Log(logMsg)
		}
	}
}