package main

import (
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func createFiles() (dir string) {
	dir, err := ioutil.TempDir("", "dummytest")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Temp dir created:", dir)
	for i:= 0; i < 50; i++ {
		tmpfile, err := ioutil.TempFile(dir, "dummyfile")
		if err != nil {
			log.Fatal(err)
		}
		if err := tmpfile.Close() ; err != nil {
			log.Fatal(err)
		}
		log.Println("Temp file created:", tmpfile.Name())
	}
	return dir
}

func main() {
	dir := createFiles()
	
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGHUP)
	sig := <- c
	log.Println("Recevied signal:", sig)
	err := os.RemoveAll(dir)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Deleted:", dir)
}
