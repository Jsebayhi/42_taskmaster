package main

import (
	"log"
	"os"
	"strconv"
	"strings"
	"syscall"
	"time"
)

func actionTaskProcessStart(taskProcess *TaskProcess) error {
	if taskProcess.IsActive() == false {
		processParameters := strings.Split(taskProcess.taskConfig.Command, " ")
		if err := actionTaskProcessLauncher(taskProcess, processParameters); err != nil {
			return err
		}
		if !taskProcess.doHaveAProcessManager {
			go r_actionTaskProcessManager(taskProcess)
			taskProcess.doHaveAProcessManager = true
		}
		return nil
	}
	return ErrorNew_ErrorTaskProcessAlreadyActive(taskProcess.taskConfig.Name, taskProcess.taskProcessId)
}

func r_actionTaskProcessManager(taskProcess *TaskProcess) {
	const StartCountInitialValue = 0

	StartCount := StartCountInitialValue
	processArgs := strings.Split(taskProcess.taskConfig.Command, " ")
	for {
		StartCount++
		taskProcess.LaunchRequested.Lock()
		taskProcess.LaunchRequested.Unlock()
		status := actionTaskProcessHandler(taskProcess, processArgs)
		if taskProcess.FdOut != nil {
			taskProcess.FdOut.Close()
		}
		if taskProcess.FdErr != nil {
			taskProcess.FdErr.Close()
		}
		if status == false {
			if StartCount <= taskProcess.taskConfig.RestartTry && taskProcess.taskConfig.KeepAlive != ConfigKeepAlive_never {
				taskProcess.LaunchRequested.Lock()
				actionTaskProcessRestart(taskProcess)
			} else {
				break
			}
		} else {
			if taskProcess.action != ProcessAction_stop && taskProcess.taskConfig.KeepAlive == ConfigKeepAlive_always {
				StartCount = StartCountInitialValue
			} else {
				break
			}
		}
	}
	time.Sleep(1 * time.Second)
	taskProcess.doHaveAProcessManager = false
}

func actionTaskProcessHandler(taskProcess *TaskProcess, processParameters []string) bool {
	var err error
	const errorOccurred = false
	const gracefullReturn = true

	// Cmd Ending handling
	taskProcess.PrcStatus, err = taskProcess.Process.Wait()
	if taskProcess.action == ProcessAction_run {
		if err != nil || taskProcess.ExitCodesMatch(taskProcess.PrcStatus.Sys().(syscall.WaitStatus).ExitStatus()) != true {
			taskProcess.StatusSet(ProcessStatus_errorOccurred)
			LogProcessEnded(taskProcess.FullName(), "Error happend", taskProcess.PrcStatus.String())
			return errorOccurred
		}
		taskProcess.StatusSet(ProcessStatus_finished)
		LogProcessEnded(taskProcess.FullName(), "Finished", taskProcess.PrcStatus.String())
	} else if taskProcess.action == ProcessAction_stop {
		taskProcess.StatusSet(ProcessStatus_stopped)
		LogProcessEnded(taskProcess.FullName(), "Stopped without error", taskProcess.PrcStatus.String())
	} else if taskProcess.action == ProcessAction_kill {
		taskProcess.StatusSet(ProcessStatus_killed)
		LogProcessEnded(taskProcess.FullName(), "Killed (not stopping)", taskProcess.PrcStatus.String())
	}
	return gracefullReturn
}

func actionTaskProcessLauncher(taskProcess *TaskProcess, processParameters []string) error {
	var err error

	defer taskProcess.LaunchRequested.Unlock()

	// Check if the starter is already running in another routine to avoid interference
	g_taskMaster.LockProcessLauncher()
	defer g_taskMaster.UnLockProcessLauncher()
	LogProcessStarting(taskProcess.FullName())
	taskProcess.ActionSet(ProcessAction_run)
	taskProcess.StatusSet(ProcessStatus_startingPrep)

	// Changing directory
	var defaultDirectory string //Todo put in taskmaster conf as an option in settings.
	if taskProcess.taskConfig.DirSet != "" {
		defaultDirectory, err = os.Getwd()
		if err != nil {
			LogProcessStartAbort(taskProcess.FullName(), "Unable to get current directory")
			taskProcess.StatusSet(ProcessStatus_unableToStart)
			taskProcess.ActionSet(ProcessAction_none)
			return err
		}
		err = os.Chdir(taskProcess.taskConfig.DirSet)
		if err != nil {
			LogProcessStartAbort(taskProcess.FullName(), "Unable to get set directory to "+taskProcess.taskConfig.DirSet)
			taskProcess.StatusSet(ProcessStatus_unableToStart)
			taskProcess.ActionSet(ProcessAction_none)
			g_taskMaster.UnLockProcessLauncher()
			return err
		}
		LogProcessLauncher("Setting directory to " + taskProcess.taskConfig.DirSet)
		defer func() {
			if err := os.Chdir(defaultDirectory); err != nil {
				LogBeforePanic(taskProcess.FullName() + dash + "Unable to set directory to " + defaultDirectory)
				log.Panic(err)
			}
			LogProcessLauncher("Setting directory to " + defaultDirectory)
		}()
	}

	// change Umask
	var defaultUMask int                                       //TODO put in taskmaster conf as an option in settings.
	defaultUMask = syscall.Umask(taskProcess.taskConfig.Umask) //TODO: use the sub function to get error if an error occurred?
	LogProcessLauncher("Setting umask to " + strconv.Itoa(taskProcess.taskConfig.Umask))
	defer func() {
		syscall.Umask(defaultUMask)
		LogProcessLauncher("Setting umask to " + strconv.Itoa(defaultUMask))
	}()

	processAttributes := new(os.ProcAttr)
	if taskProcess.taskConfig.StdoutLogfile != "" {
		taskProcess.FdOut, err = os.OpenFile(taskProcess.taskConfig.StdoutLogfile, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0777)
		if err != nil {
			LogProcessStartAbort(taskProcess.FullName(), "Unable to open StdoutLogfile: "+err.Error())
			taskProcess.StatusSet(ProcessStatus_unableToStart)
			taskProcess.ActionSet(ProcessAction_none)
			return err
		}
	}
	if taskProcess.taskConfig.StderrLogfile != "" {
		taskProcess.FdErr, err = os.OpenFile(taskProcess.taskConfig.StderrLogfile, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0777)
		if err != nil {
			LogProcessStartAbort(taskProcess.FullName(), "Unable to open StderrLogfile: "+err.Error())
			taskProcess.StatusSet(ProcessStatus_unableToStart)
			taskProcess.ActionSet(ProcessAction_none)
			return err
		}
	}
	processAttributes.Files = []*os.File{nil, taskProcess.FdOut, taskProcess.FdErr}

	// Modifying env
	if len(taskProcess.taskConfig.EnvSet) != 0 {
		processAttributes.Env = os.Environ()
		for _, envVar := range taskProcess.taskConfig.EnvSet {
			processAttributes.Env = append(processAttributes.Env, envVar)
		}
	}

	// Starting process
	taskProcess.Process, err = os.StartProcess(processParameters[0], processParameters, processAttributes)
	if err != nil {
		LogProcessStartAbort(taskProcess.FullName(), "Unable to start: "+err.Error())
		taskProcess.StatusSet(ProcessStatus_unableToStart)
		taskProcess.ActionSet(ProcessAction_none)
		return err
	}
	taskProcess.ProcessStartedIni()
	return nil
}
