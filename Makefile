NAME = 42_taskmaster

DEPENDANCIES =  gopkg.in/yaml.v2\
                gopkg.in/abiosoft/ishell.v1

TEST_SOURCE_PATH = tests/sources
TEST_BIN_PATH = tests/bin
TEST_C = pgm_segfault
TEST_GO = 	pgm_wait10sec\
			pgm_wait1sec\
			pgm_sleepInfinity\
			pgm_createTempFiles
TEST_SH = cmd_env.sh

default: project test


project: project_fclean project_getDependancies project_build

project_getDependancies:
	@for dep in $(DEPENDANCIES) ; do\
		echo fetching $$dep ; \
		go get $$dep ; \
	done

project_build:
	go build -o $(NAME)

project_fclean:
	rm -f $(NAME)

project_re: project_fclean project

test: test_clean test_build

test_build:
	@echo "Creating bin folder for compiled tests"
	mkdir -p $(TEST_BIN_PATH)
	@echo "Tests written in C."
	@for src in $(TEST_C) ; do \
		echo "Compiling $$src ." ; \
		gcc $(TEST_SOURCE_PATH)/$$src".c" -o $(TEST_BIN_PATH)/$$src".bin" ; \
	done
	@echo "Tests written in GO."
	@for src in $(TEST_GO) ; do \
		echo "Compiling $$src." ; \
		go build -o $(TEST_BIN_PATH)/$$src".bin" $(TEST_SOURCE_PATH)/$$src".go" ; \
	done
	@echo "Tests written in BASH."
	@for src in $(TEST_SH) ; do \
		echo "Copying $$src." ; \
		cp $(TEST_SOURCE_PATH)/$$src $(TEST_BIN_PATH)/$$src ; \
	done

test_clean:
	rm -rf $(TEST_BIN_PATH)

re: test project_re
