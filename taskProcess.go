package main

import (
	"os"
	"strconv"
	"sync"
	"time"
)

type ProcessStatus string

const (
	ProcessStatus_neverLaunched ProcessStatus = "NEVER_LAUNCHED"
	ProcessStatus_starting      ProcessStatus = "STARTING"
	ProcessStatus_startingPrep  ProcessStatus = "STARTING_PREPARATION"
	ProcessStatus_running       ProcessStatus = "RUNNING"
	ProcessStatus_finished      ProcessStatus = "FINISHED"
	ProcessStatus_stopped       ProcessStatus = "STOPPED"
	ProcessStatus_killed        ProcessStatus = "KILLED"
	ProcessStatus_unableToStart ProcessStatus = "UNABLE_TO_START"
	ProcessStatus_errorOccurred ProcessStatus = "ERROR_OCCURRED"
)

type ProcessAction string

const (
	ProcessAction_run  ProcessAction = "RUN"
	ProcessAction_stop ProcessAction = "STOP"
	ProcessAction_kill ProcessAction = "KILL"
	ProcessAction_none ProcessAction = "NONE"
)

func TaskProcessNew(config TaskConfig, processId int) *TaskProcess {
	process := TaskProcess{taskConfig: config,
		Process:               nil,
		status:                ProcessStatus_neverLaunched,
		action:                ProcessAction_none,
		doHaveAProcessManager: false}
	process.ProcessIdSet(processId)
	return &process
}

type TaskProcess struct {
	taskConfig TaskConfig

	taskProcessId         int
	taskProcessIdAsString string
	Process               *os.Process
	PrcStatus             *os.ProcessState
	status                ProcessStatus //Last known status.
	FdOut                 *os.File
	FdErr                 *os.File
	TimeStarted           time.Time
	TimeToLaunch          time.Duration
	timeAlive             time.Duration
	action                ProcessAction //Last reported action started.
	doHaveAProcessManager bool
	LaunchRequested       sync.Mutex
}

func (taskProcess *TaskProcess) ProcessStartedIni() {
	taskProcess.TimeStarted = time.Now()
	taskProcess.StatusSet(ProcessStatus_starting)
	taskProcess.TimeAliveSet()
}

func (taskProcess *TaskProcess) FullName() string {
	return taskProcess.taskConfig.Name + taskProcess.ProcessIdAsString()
}

func (taskProcess *TaskProcess) IsActive() bool {
	return taskProcess.status == ProcessStatus_running || taskProcess.status == ProcessStatus_starting
}

func (taskProcess *TaskProcess) ExitCodesMatch(code int) bool {
	if len(taskProcess.taskConfig.ExitCodes) == 0 {
		return true
	}
	for _, exitCode := range taskProcess.taskConfig.ExitCodes {
		if exitCode == code {
			return true
		}
	}
	return false
}

//GETTER SETTER

func (taskProcess *TaskProcess) ProcessId() int {
	return taskProcess.taskProcessId
}

func (taskProcess *TaskProcess) ProcessIdAsString() string {
	return taskProcess.taskProcessIdAsString
}

func (taskProcess *TaskProcess) ProcessIdSet(processId int) {
	taskProcess.taskProcessId = processId
	taskProcess.taskProcessIdAsString = strconv.Itoa(processId)
}

func (taskProcess *TaskProcess) TimeAliveSet() {
	if taskProcess.IsActive() {
		taskProcess.timeAlive = time.Now().Sub(taskProcess.TimeStarted)
	}
}

func (taskProcess *TaskProcess) TimeAlive() time.Duration {
	return taskProcess.timeAlive
}

func (taskProcess *TaskProcess) ActionSet(action ProcessAction) {
	taskProcess.action = action
}

func (taskProcess *TaskProcess) StatusSet(status ProcessStatus) {
	taskProcess.status = status
}

func (taskProcess *TaskProcess) Status() ProcessStatus {
	return taskProcess.status
}
