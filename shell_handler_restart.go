package main

import (
	"strings"

	"gopkg.in/abiosoft/ishell.v1"
)

const (
	ShellCommand_restart ShellCommandName = "restart"
	HandlerResetSyntax   string           = string(ShellCommand_restart) + " " + ArgProcessExtractorSyntax
)

func handlerRestart(shell *ishell.Shell) {
	HandlerSyntax[ShellCommand_restart] = HandlerResetSyntax
	shell_Register(shell, ShellCommand_restart, func(args ...string) (string, error) {
		if len(args) <= 0 {
			return handlerSyntaxError(ShellCommand_restart), nil
		}
		tasksProcesses, msgs := ArgExtractorProcess(args...)
		for _, taskProcess := range tasksProcesses {
			msgs = append(msgs, "Restarting "+taskProcess.FullName()+" "+taskProcess.ProcessIdAsString()+".")
			taskProcess.LaunchRequested.Lock()
			actionTaskProcessRestart(taskProcess)
		}
		return strings.Join(msgs, "\n"), nil
	})
}
