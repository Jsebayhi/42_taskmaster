package main

import (
	"reflect"
)

func ProcessesToKillKeepRestart(oldTasksConfig, newTasksConfig map[string]*TaskConfig) (tasksToKill, tasksToKeep, tasksToStart, tasksToRestart []string) {
	channel := make(chan []string)
	go func (){
		var exist bool
		for taskConfigName := range newTasksConfig {
			exist = false
			for oldTaskConfigName := range oldTasksConfig {
				if taskConfigName == oldTaskConfigName {
					exist = true
					break
				}
			}
			if !exist {
				tasksToStart = append(tasksToStart, taskConfigName)
			}
		}
		channel <- tasksToStart
	}()

	for oldKey, oldVal := range oldTasksConfig {
		if newVal, doesExist := newTasksConfig[oldKey]; doesExist {
			if reflect.DeepEqual(newVal, oldVal) == true {
				tasksToKeep = append(tasksToKeep, oldKey)
			} else {
				tasksToRestart = append(tasksToRestart, oldKey)
			}
		} else {
			tasksToKill = append(tasksToKill, oldKey)
		}
	}
	tasksToStart = <- channel
	return tasksToKill, tasksToKeep, tasksToStart, tasksToRestart
}
