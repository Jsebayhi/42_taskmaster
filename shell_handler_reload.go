package main

import (
	"gopkg.in/abiosoft/ishell.v1"
	"os"
	"errors"
)

const (
	ShellCommand_reload ShellCommandName = "reload"
	HandlerReloadSyntax string = "reload <pathToFile.yaml>"
	RegisteredConfFileDoesNotExist string = "Registered conf file does not exist: "
	TasksConfSuffix = ".yaml"
)

// Reload the configuration in the taskmaster known path or replace this path the provided one before loading this new conf.
func handlerReload(shell *ishell.Shell) {
	HandlerSyntax[ShellCommand_reload] = HandlerReloadSyntax
	shell_Register(shell, ShellCommand_reload, func(args ...string) (string, error) {
		if len(args) > 1 {
			return handlerSyntaxError(ShellCommand_reload), nil
		}
		if len(args) == 1 {
			if err := g_taskMaster.TaskConfigFileNameSet(args[0]); err != nil {
				return "", err
			}
		}
		if _, err := os.Stat(g_taskMaster.TaskConfigFileName()); os.IsNotExist(err) {
			return "", errors.New(RegisteredConfFileDoesNotExist + ": " + g_taskMaster.TaskConfigFileName() + ".")
		} else {
			actionTaskProcessReload()
			return "Reloading " + g_taskMaster.TaskConfigFileName() + ".", nil
		}

	})
}
