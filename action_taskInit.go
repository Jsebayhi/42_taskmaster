package main

import "log"

func actionTaskInit(taskConfig *TaskConfig) error {
	taskConfig.FallBackDefault()
	if _, exist := g_taskMaster.TasksProcesses[taskConfig.Name]; exist {
		return ErrorTaskAlreadyExist
	}
	g_taskMaster.TasksProcesses[taskConfig.Name] = map[int]*TaskProcess{}
	for processId := 0; processId < taskConfig.ProcessNb; processId++ {
		taskProcess := TaskProcessNew(*taskConfig, processId)
		g_taskMaster.TasksProcesses[taskConfig.Name][taskProcess.ProcessId()] = taskProcess
		if taskProcess.taskConfig.StartAtLaunch == true {
			taskProcess.LaunchRequested.Lock()
			actionTaskProcessStart(taskProcess)
		}
	}
	return nil
}

func actionTasksInit() {
	var err error
	for _, taskConfig := range g_taskMaster.TasksConfigs {
		if err = actionTaskInit(taskConfig); err != nil {
			log.Panic(err)
		}
	}
}
