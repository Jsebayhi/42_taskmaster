package main

import (
	"gopkg.in/abiosoft/ishell.v1"
)

func shellInit(prompt string, welcomeMsg string) *ishell.Shell {

	shell := ishell.New()
	shell.Println(welcomeMsg)
	shell.SetPrompt(prompt)
	handlerInit(shell)
	return shell
}
