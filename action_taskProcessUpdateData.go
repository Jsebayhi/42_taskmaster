package main

import (
	"time"
)

func actionCmdsProcessUpdateData(taskProcess *TaskProcess) {
	taskProcess.TimeAliveSet()
	// Updating status
	if taskProcess.status == ProcessStatus_starting {
		taskProcess.TimeToLaunch = time.Now().Sub(taskProcess.TimeStarted)
		if taskProcess.status == ProcessStatus_starting && taskProcess.TimeToLaunch >= taskProcess.taskConfig.TimeToLaunchExpected {
			taskProcess.StatusSet(ProcessStatus_running)
		}
	}
}