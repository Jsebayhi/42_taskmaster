package main

import "log"

func CopyTasksProcesses(m map[string]map[int]*TaskProcess) (newMap map[string]map[int]*TaskProcess) {
	newMap = map[string]map[int]*TaskProcess{}
	for taskName, taskProcesses := range m {
		newMap[taskName] = map[int]*TaskProcess{}
		for taskProcessId, taskProcess := range taskProcesses {
			newMap[taskName][taskProcessId] = taskProcess
		}
	}
	return
}

func CopyTasksConfigs(m map[string]*TaskConfig) (newMap map[string]*TaskConfig) {
	newMap = map[string]*TaskConfig{}
	for taskName, taskConfig := range m {
		newMap[taskName] = taskConfig
	}
	return
}

func actionTaskProcessReload() error {
	// Back up old map of configs and processes
	oldTasksProcesses := CopyTasksProcesses(g_taskMaster.TasksProcesses)
	oldConf := CopyTasksConfigs(g_taskMaster.TasksConfigs)

	LogReload("ReLoading conf: %v", g_taskMaster.TaskConfigFileName())
	if err := g_taskMaster.LoadTaskConfigs(); err != nil {
		g_taskMaster.TasksConfigs = oldConf
		return err
	}
	LogReload("ReLoading conf: %v", g_taskMaster.TasksConfigs)
	LogReload("DEBUG: new map of tasksConfigs: %v", g_taskMaster.TasksConfigs)

	// make diff
	tasksToKill, tasksToKeep, tasksToStart, tasksToRestart := ProcessesToKillKeepRestart(oldConf, g_taskMaster.TasksConfigs)

	LogReload("Killing removed tasks: %v", tasksToKill)
	for _, taskName := range tasksToKill {
		if task, doesExist := oldTasksProcesses[taskName]; !doesExist {
			g_taskMaster.TasksConfigs = oldConf
			g_taskMaster.TasksProcesses = oldTasksProcesses
			log.Panic("TaskProcessReload - taskProcessName not found in taskMaster processes map.")
		} else {
			for _, taskProcess := range task {
				actionTaskProcessStop(taskProcess)
			}
		}
	}

	LogReload("Killing updated tasks' processes: %v", tasksToRestart)
	for _, taskName := range tasksToRestart {
		if task, doesExist := oldTasksProcesses[taskName]; !doesExist {
			g_taskMaster.TasksConfigs = oldConf
			g_taskMaster.TasksProcesses = oldTasksProcesses
			log.Panic("TaskProcessReload - taskProcessName not found in taskMaster processes map.")
		} else {
			for _, taskProcess := range task {
				actionTaskProcessStop(taskProcess)
			}
		}
	}

	// make new map of task and process
	g_taskMaster.TasksProcesses = map[string]map[int]*TaskProcess{}
	LogReload("DEBUG: new map of tasksProcess: %v", g_taskMaster.TasksProcesses)

	// cp processes to keep in the new map
	for _, taskName := range tasksToKeep {
		if taskProcesses, doesExist := g_taskMaster.TasksProcesses[taskName]; doesExist {
			g_taskMaster.TasksConfigs = oldConf
			g_taskMaster.TasksProcesses = oldTasksProcesses
			log.Panic("TaskProcessReload - taskProcessName not found in taskMaster processes map.")
		} else {
			g_taskMaster.TasksProcesses[taskName] = taskProcesses
		}
	}

	LogReload("Start new processes: %v", tasksToStart)
	for _, taskName := range tasksToStart {
		if taskConfig, doesExist := g_taskMaster.TasksConfigs[taskName]; !doesExist {
			g_taskMaster.TasksConfigs = oldConf
			g_taskMaster.TasksProcesses = oldTasksProcesses
			log.Panicf("TaskProcessReload - taskProcessName '%v' not found in taskMaster processes map", taskName)
		} else {
			if err := actionTaskInit(taskConfig); err != nil {
				log.Panic(err)
			}
		}
	}

	LogReload("Start updated tasks' processes: %v", tasksToRestart)
	for _, taskName := range tasksToRestart {
		if taskConfig, doesExist := g_taskMaster.TasksConfigs[taskName]; !doesExist {
			g_taskMaster.TasksConfigs = oldConf
			g_taskMaster.TasksProcesses = oldTasksProcesses
			log.Panicf("TaskProcessReload - taskProcessName '%v' not found in taskMaster config map", taskName)
		} else {
			if err := actionTaskInit(taskConfig); err != nil {
				log.Panic(err)
			}
		}
	}
	return nil
}
