package main

import (
	"fmt"
	"errors"
)

var (
	ErrorTaskAlreadyExist error = errors.New("Task doesn't exist")//TODO: make a proper error out of it.
)

// TaskProcessAlreadyActive
type ErrorTaskProcessAlreadyActive struct {
	TaskName  string
	ProcessId int
	msg       string
}
func (err *ErrorTaskProcessAlreadyActive) Error() string {
	return err.msg
}
func ErrorNew_ErrorTaskProcessAlreadyActive(taskName string, processId int) error {
	return &ErrorTaskProcessAlreadyActive{TaskName: taskName,
		ProcessId: processId,
		msg: fmt.Sprintf("Process %s %d already active.", taskName, processId)}
}

// UnableToFindProcess
type ErrorUnableToFindTaskProcess struct {
	TaskName  string
	ProcessId int
	msg       string
}
func (err *ErrorUnableToFindTaskProcess) Error() string {
	return err.msg
}
func ErrorNew_ErrorUnableToFindTaskProcess(taskName string, processId int) error {
	return &ErrorUnableToFindTaskProcess{TaskName: taskName,
		ProcessId: processId,
		msg: fmt.Sprintf("Unable to find process: %s %d.", taskName, processId)}
}

// UnableToFindTask
type ErrorUnableToFindTask struct {
	TaskName  string
	msg       string
}
func (err *ErrorUnableToFindTask) Error() string {
	return err.msg
}
func ErrorNew_ErrorUnableToFindTask(taskName string) error {
	return &ErrorUnableToFindTask{TaskName: taskName,
		msg: fmt.Sprintf("Unable to find task: %s.", taskName)}
}