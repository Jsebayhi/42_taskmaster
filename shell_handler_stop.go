package main

import (
	"gopkg.in/abiosoft/ishell.v1"
	"strings"
)

const (
	ShellCommand_stop ShellCommandName = "stop"
	HandlerStopSyntax string = string(ShellCommand_stop)+" "+ArgProcessExtractorSyntax
)

func handlerStop(shell *ishell.Shell) {
	HandlerSyntax[ShellCommand_stop] = HandlerStopSyntax
	shell_Register(shell, ShellCommand_stop, func(args ...string) (string, error) {
		if len(args) <= 0 {
			return handlerSyntaxError(ShellCommand_stop), nil
		}
		tasksProcesses, msgs := ArgExtractorProcess(args...)
		for _, taskProcess := range tasksProcesses {
			actionTaskProcessStop(taskProcess)
			msgs = append(msgs, args[0] + " stopped.")
		}
		return strings.Join(msgs, "\n"), nil
	})
}
