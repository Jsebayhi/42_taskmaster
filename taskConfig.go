package main

import (
	"time"
)

type ConfigKeepAlive string
const (
	ConfigKeepAlive_always ConfigKeepAlive = "always"
	ConfigKeepAlive_never ConfigKeepAlive =  "never"
	ConfigKeepAlive_unexpected ConfigKeepAlive = "unexpected"
)

type ConfigSignals string
const (
	ConfigSignals_term ConfigSignals = "TERM"
	ConfigSignals_stop ConfigSignals = "STOP"
	ConfigSignals_quit ConfigSignals = "QUIT"
	ConfigSignals_int ConfigSignals = "INT"
	ConfigSignals_hup ConfigSignals = "HUP"
)

type TaskConfig struct {
	Name                 string			`yaml:"name"`
	Command              string			`yaml:"cmd"`
	ProcessNb            int			`yaml:"num_process"`
	StartAtLaunch        bool			`yaml:"autostart"`
	TimeToLaunchExpected time.Duration		`yaml:"start_time"`
	KeepAlive            ConfigKeepAlive		`yaml:"keep_alive"`
	RestartTry           int			`yaml:"start_retries"`
	ExitCodes            []int			`yaml:"exit_codes", flow`
	StopSignals          ConfigSignals		`yaml:"stop_signal"`
	StopWaitSecs         time.Duration		`yaml:"stop_time"`
	EnvSet               []string			`yaml:"env", flow`
	StdoutLogfile        string			`yaml:"stdout"` //if empty then discarded
	//StdoutLogfileMaxbytes int
	//StdoutLogfileBackups  int
	StderrLogfile        string			`yaml:"stderr"` //if empty then discarded
	//StderrLogfileMaxbytes int
	//StderrLogfileBackups  int
	DirSet               string			`yaml:"dir"`
	Umask                int			`yaml:"umask"` //default value: 022
	//	logfile_maxbytes = 50MB
	//	logfile_backups=10
}

func (taskConfig *TaskConfig) FallBackDefault() {
	if taskConfig.KeepAlive == "" {
		taskConfig.KeepAlive = ConfigKeepAlive_never
	}
	if len(taskConfig.ExitCodes) == 0 {
		taskConfig.ExitCodes = append(taskConfig.ExitCodes, 0)
	}
	if taskConfig.StopSignals == "" {
		taskConfig.StopSignals = ConfigSignals_term
	}
	if taskConfig.StopWaitSecs.String() == "0s" {
		taskConfig.StopWaitSecs, _ = time.ParseDuration("1s")
	}
	if taskConfig.TimeToLaunchExpected.String() == "0s" {
		taskConfig.TimeToLaunchExpected, _ = time.ParseDuration("1s")
	}
	if taskConfig.Umask == 0 {
		taskConfig.Umask = 022
	}
}
