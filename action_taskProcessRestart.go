package main

import (
	"time"
)

func actionTaskProcessRestart(taskProcess *TaskProcess) {
	go r_actionTaskProcessRestart(taskProcess)
}

func r_actionTaskProcessRestart(taskProcess *TaskProcess) {
	LogProcessRestarting(taskProcess.FullName())
	actionTaskProcessStop(taskProcess)
	for taskProcess.IsActive() {
		time.Sleep(10 * time.Nanosecond)
	}
	actionTaskProcessStart(taskProcess)
}
