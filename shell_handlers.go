package main

import (
	"gopkg.in/abiosoft/ishell.v1"
	"strconv"
)

const (
	OutputEventSeparator string = "\n"
	ShellCommand_parameterAll string = "--all"
	ShellCommand_parameterTask string = "--task"
	flagTask string = "task"
	flagProcessId string = "processId"
	ArgProcessExtractorSyntax string = "["+ShellCommand_parameterAll+"] ["+ShellCommand_parameterTask+" task"+" [Id]]"
)

// Type for handler name declaration. Any new handler must declare it's name as this type
//  in order to register it's specific syntax usage.
// Allow to control what is being asked to the HandlerSyntax at build time.
type ShellCommandName string
var (
	// Any new handler has to register it's command syntax usage in this map.
	HandlerSyntax = map[ShellCommandName]string{}
)

func handlerSyntaxError(handlerName ShellCommandName) string {
	return "Syntax error: " + HandlerSyntax[handlerName] + "."
}

func handlerInit(shell *ishell.Shell) {
	handlerHelp(shell)
	handlerStatus(shell)
	handlerStart(shell)
	handlerStop(shell)
	handlerRestart(shell)
	handlerReload(shell)
	handlerSignal(shell)
}

func shell_Register(shell *ishell.Shell, shellCommandName ShellCommandName, handler func(args ...string) (string, error)) {
	shell.Register(string(shellCommandName), handler)
}

//TODO add check if process already fetch.
func ArgExtractorProcess(args ...string) (tasksProcesses []*TaskProcess, msgs []string) {
	var flag string
	var task string
	loopArg:
	for index, arg := range args {
		switch arg {
		case ShellCommand_parameterAll:
			tasksProcesses = g_taskMaster.ProcessGetAll()
			break loopArg
		case ShellCommand_parameterTask:
			flag = flagTask
		default:
			switch flag {
			case flagProcessId:
				prcId, err := strconv.Atoi(arg)
				if err == nil {
					if prc, err := g_taskMaster.FindProcess(task, prcId); err != nil {
						msgs = append(msgs, err.Error())
					} else {
						tasksProcesses = append(tasksProcesses, prc)
					}
				} else {
					msgs = append(msgs, err.Error())
				}
			default:
				task = arg
				flag = flagProcessId
				if len(args) == index + 1 || args[index + 1] == ShellCommand_parameterTask {
					if prcs, err := g_taskMaster.TasksProcessesGetAll(task); err != nil {
						msgs = append(msgs, err.Error())
					} else {
						tasksProcesses = append(tasksProcesses, prcs...)
					}
				}
			}
		}
	}
	return
}
