package main

import (
	"strings"

	"gopkg.in/abiosoft/ishell.v1"
)

const (
	ShellCommand_start ShellCommandName = "start"
	HandlerStartSyntax string           = string(ShellCommand_start) + " " + ArgProcessExtractorSyntax
)

func handlerStart(shell *ishell.Shell) {
	HandlerSyntax[ShellCommand_start] = HandlerStartSyntax
	shell_Register(shell, ShellCommand_start, func(args ...string) (string, error) {
		if len(args) <= 0 {
			return handlerSyntaxError(ShellCommand_start), nil
		}
		tasksProcesses, msgs := ArgExtractorProcess(args...)
		for _, taskProcess := range tasksProcesses {
			if taskProcess.IsActive() {
				msgs = append(msgs, "Already launched "+taskProcess.taskConfig.Name+" "+taskProcess.ProcessIdAsString()+".")
			} else {
				taskProcess.LaunchRequested.Lock()
				if err := actionTaskProcessStart(taskProcess); err != nil {
					msgs = append(msgs, err.Error())
				} else {
					msgs = append(msgs, "Starting "+taskProcess.taskConfig.Name+" "+taskProcess.ProcessIdAsString()+".")
				}
			}
		}
		return strings.Join(msgs, "\n"), nil
	})
}
