package main

import (
	"gopkg.in/abiosoft/ishell.v1"
	"strings"
)

const (
	ShellCommand_help ShellCommandName = "help"
)

func handlerHelp(shell *ishell.Shell) {
	shell_Register(shell, ShellCommand_help, func(args ...string) (string, error) {
		var commands []string
		var msgs []string

		commands = shell.Commands()
		msgs = append(msgs, "Help section:")
		for _, command := range commands {
			syntax, exist := HandlerSyntax[ShellCommandName(command)]
			if !exist {
				syntax = command
			}
			msgs = append(msgs, dash + syntax)
		}
		return strings.Join(msgs, OutputEventSeparator), nil
	})
}