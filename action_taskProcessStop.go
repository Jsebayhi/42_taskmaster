package main

import (
	"syscall"
	"time"
)

func actionTaskProcessStopAll() bool {
	for _, tasks := range g_taskMaster.TasksProcesses {
		for _, taskProcess := range tasks {
			if taskProcess.IsActive() {
				actionTaskProcessStop(taskProcess)
				_, _ = taskProcess.Process.Wait()
			}
		}
	}
	return true
}

func actionTaskProcessStop(taskProcess *TaskProcess) {
	if !taskProcess.IsActive() {
		return
	}
	taskProcess.ActionSet(ProcessAction_stop)
	switch taskProcess.taskConfig.StopSignals {
	case ConfigSignals_quit:
		LogProcessStopping(taskProcess.FullName(), ConfigSignals_quit)
		taskProcess.Process.Signal(syscall.SIGQUIT)
	case ConfigSignals_int:
		LogProcessStopping(taskProcess.FullName(), ConfigSignals_int)
		taskProcess.Process.Signal(syscall.SIGINT)
	case ConfigSignals_stop:
		LogProcessStopping(taskProcess.FullName(), ConfigSignals_stop)
		taskProcess.Process.Signal(syscall.SIGSTOP)
	case ConfigSignals_hup:
		LogProcessStopping(taskProcess.FullName(), ConfigSignals_hup)
		taskProcess.Process.Signal(syscall.SIGHUP)
	default:
		LogProcessStopping(taskProcess.FullName(), ConfigSignals_term)
		taskProcess.Process.Signal(syscall.SIGTERM)
	}
	go r_actionTaskProcessKill(taskProcess)
}

func r_actionTaskProcessKill(taskProcess *TaskProcess) {
	time.Sleep(taskProcess.taskConfig.StopWaitSecs)
	if !taskProcess.IsActive() {
		return
	}
	LogProcessKilling(taskProcess.FullName(), "Allowed stopping time elapsed")
	taskProcess.ActionSet(ProcessAction_kill)
	taskProcess.Process.Kill()
}