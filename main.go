package main

import (
	"gopkg.in/abiosoft/ishell.v1"
	"flag"
	"os"
	"os/signal"
	"syscall"
)

var g_taskMaster TaskMaster

const (
	logsFileDefault string = "/tmp/logs/taskMasterLogs"

	taskConfPathParameterName string = "taskConfFile"
	taskConfPathParameterUsage string = taskConfPathParameterName + "pathToFile"
	taskConfPathDefault string = "tests/conf.yaml"
)

func init() {
	var tasksConfigsPath string
	flag.StringVar(&tasksConfigsPath, taskConfPathParameterName, taskConfPathDefault, taskConfPathParameterUsage)
	flag.Parse()
	g_taskMaster.Init(tasksConfigsPath)
	Log("Initialization over.")
}

func main() {

	var shell *ishell.Shell

	//init
	defer func () {
		actionTaskProcessStopAll()
		g_taskMaster.Terminate()
	}()

	//sighup signal handling
	go SignalHandling(syscall.SIGHUP)

	//shell
	shell = shellInit("$> ", "Welcome to Taskmaster shell")
	shell.Start()
}

func SignalHandling(sig os.Signal) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, sig)
	for sig := range c {
		LogSignalReceived(sig)
		actionTaskProcessReload()
	}

}