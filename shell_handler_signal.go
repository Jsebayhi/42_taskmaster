package main

import (
	"strings"
	"syscall"

	"gopkg.in/abiosoft/ishell.v1"
)

const (
	SIGKILL string = "SIGKILL"
	SIGTERM string = "SIGTERM"
	SIGINT string = "SIGINT"
	SIGQUIT string = "SIGQUIT"
	HUP string = "SIGHUP"
)

const (
	ShellCommand_signal ShellCommandName = "signal"
	HandlerSignalSyntax string = string(ShellCommand_signal) + " SIGNAL "+ArgProcessExtractorSyntax
)

func handlerSignal(shell *ishell.Shell) {
	HandlerSyntax[ShellCommand_signal] = HandlerSignalSyntax
	shell_Register(shell, ShellCommand_signal, func(args ...string) (string, error) {
		if len(args) < 2 {
			return handlerSyntaxError(ShellCommand_signal), nil
		}
		tasksProcesses, msgs := ArgExtractorProcess(args[1:]...)
		loop_process:
		for _, taskProcess := range tasksProcesses {
			if taskProcess.IsActive() {
				switch strings.ToUpper(args[0]) {
				case SIGKILL:
					taskProcess.Process.Kill()
				case SIGTERM:
					taskProcess.Process.Signal(syscall.SIGTERM)
				case SIGINT:
					taskProcess.Process.Signal(syscall.SIGINT)
				case SIGQUIT:
					taskProcess.Process.Signal(syscall.SIGQUIT)
				case HUP:
					taskProcess.Process.Signal(syscall.SIGHUP)
				default:
					return "Unknown signal: " + strings.ToUpper(args[0]), nil
					break loop_process
				}
				msgs = append(msgs, "Signal " + args[0] + " sent to " + taskProcess.taskConfig.Name + " " + taskProcess.taskProcessIdAsString + ".")
			} else {
				msgs = append(msgs, taskProcess.taskConfig.Name + " " + taskProcess.taskProcessIdAsString + " not active.")
			}
		}
		return strings.Join(msgs, "\n"), nil
	})
}
