package main

import (
	"log"
	"os"
)

func Log(msg string) {
	log.Println(msg)
}

func LogInitializingError(component string, msg string) {
	log.Println("InitializingError: " + component + dash + msg + ".")
}

func LogProcessLauncher(msg string) {
	log.Println("ProcessLauncher: " + msg + ".")
}

func LogProcessRestarting(processName string) {
	log.Println("ProcessStarting: " + processName + ".")
}

func LogProcessStarting(processName string) {
	log.Println("ProcessStarting: " + processName + ".")
}

func LogProcessStartAbort(processName string, msg string) {
	log.Println("ProcessStartAborted: " + processName + dash + msg + ".")
}

func LogProcessEnded(processName string, msg string, prcStatus string) {
	log.Println("ProcessEnded: " + processName + dash + msg + dash + prcStatus + ".")
}

func LogProcessStopping(processName string, signalBeingUsed ConfigSignals) {
	log.Println("ProcessStopping: " + processName + dash + " signal being used: " + string(signalBeingUsed) + ".")
}

func LogProcessKilling(processName string, reason string) {
	log.Println("ProcessKilling: " + processName + dash + reason + ".")
}

func LogReload(msg string, optionalStuffs ...interface{}) {
	log.Printf("Reloading: " + msg + ".", optionalStuffs...)
}

func LogBeforePanic(msg string) {
	log.Println("BeforePanic: " + msg + ".")
}

func LogSignalReceived(signal os.Signal) {
	log.Printf("Signal received: %v.", signal)
}